#!/usr/bin/env bash

###################################################################
#Script Name	: R-U-DNS
#Description	: Remove Unneeded DNS from /etc/resolv.conf
#Author       	: Walddys Emmanuel Dorrejo Céspedes
#Email         	: dev.dorrejo@gmail.com
###################################################################

# Verbose print
set +x
set +v

# Initialization of Variables
NIC=
SSID=
SSID_BAD=$(sudo nmcli device wifi | grep "\*" | awk '{print $3}')

# Getting ip from `ip route` command
ips_route=$(ip route | awk '/default/ { print $3 }')

# Getting ips from '/etc/resolv.conf' and putting on an array
ips_resolv=()
while read -r; do
    ips_resolv+=("$REPLY")
done < <(grep nameserver /etc/resolv.conf | sed 's/^.* //')

# Getting wireless and home SSID name and remembering
first_run_variables() {
    if [ "$NIC" == '' ]; then
        NIC_NAME=$(sudo /sbin/hwinfo | awk -v RS='' -v ORS='\n\n' '/WLAN/' | grep -m1 -i 'Device File' | sed 's/^.*:.//')
        sed -i "0,/NIC=/s//&""${NIC_NAME}""/" "$(readlink -f "${BASH_SOURCE}")"
    fi

    # getting HOME SSID
    if [ "$SSID" == '' ]; then
        SSID_HOME=$(nmcli device wifi | grep "\*" | awk '{print $3}')
        sed -i "0,/SSID=/s//&""$SSID_HOME"/"" "$(readlink -f "${BASH_SOURCE}")"
    fi
}

first_run_variables

if [ "$SSID_BAD" == "$SSID" ]; then
    for f in "${ips_resolv[@]}"; do
        if [ "$ips_route" != "$(echo $f)" ]; then
            echo "$ips_route" "$f"
            sudo sed -i "/$f/d" /etc/resolv.conf
        fi
    done
fi
